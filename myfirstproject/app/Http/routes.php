
<?php

//=================================
//STATIC PAGES ====================
//=================================

// show a static view for your home page (app/resources/views/home.blade.php)
Route::resource('/', 'HomeController');


//about page (app/resources/views/skills.blade.php)
Route::resource('skills', 'SkillsController');

//resume page (app/resources/views/resume.blade.php)
Route::resource('resume', 'ResumeController');

//contact page (app/resources/views/contact.blade.php)
Route::resource('contact', 'ContactController');




Route::group(['middle' => ['web']], function() {

   Route::resource('skills', 'SkillController');
});
