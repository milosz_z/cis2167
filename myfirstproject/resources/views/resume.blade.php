@extends('layouts.master')
@section('title', 'resume')
@section('content')
    <h1>MY SKILLS</h1>
    <p>Web designer</p>
    <p>Web developer</p>
    <p>User experience designer</p>
    <p>Data scientist</p>

    <h2>My Skills</h2>
    <section>
        @if (isset ($skills))

          <table class="table table-striped table-bordered">
              <thead>
                  <tr>
                      <td>Skill</td>
                      <td>Detail</td>
                      <td>Rating 0 - 5</td>
                  </tr>
              </thead>
              <tbody>
                  @foreach ($skills as $skill)
                      <tr>
                          <td>{{ $skill->title }}</td>
                          <td>{{ $skill->detail }}</td>
                          <td>
                              @for ($i = 0; $i < $skill->stars; $i++)
                                *
                              @endfor
                          </td>
                      </tr>

                  @endforeach
              </tbody>
          </table>
          @else
            <p> No skills added yet </p>
          @endif
    </section>



  {{--<section>
      @if (isset ($skills))

          <ul>
              @foreach ($skills as $skill)
                  <li>{{ $skill->title }}</li>
              @endforeach
          </ul>
      @else
          <p> no skills added yet </p>
      @endif
  </section>--}}
  @endsection
