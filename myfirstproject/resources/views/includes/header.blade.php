<nav class="navbar navbar-inverse navbar-fixed-top">
     <div class="container-fluid">
          <ul class="nav navbar-nav">
             <li class="active"><a href="/">Home</a></li>
             <li><a href="/resume">Resume</a></li>
             <li><a href="/skills">Skills</a></li>
             <li><a href="/contact">Contact</a></li>
         </ul>
     </div>
 </nav>
