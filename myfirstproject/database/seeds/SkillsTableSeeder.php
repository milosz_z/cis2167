<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([
          ['id' => 1, 'title' => "PHP", 'detail' => "Learning to code PHP using vanilla and frameworks like Laravel", 'stars' => 2],
          ['id' => 2, 'title' => "HTML", 'detail' => "writing semantic html5", 'stars' => 4],
          ['id' => 3, 'title' => "CSS", 'detail' => "Styling html using CSS and CSS3", 'stars' => 4],
        ]);
    }
}
