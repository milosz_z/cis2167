<?php

use Illuminate\Database\Seeder;

class AppearancesTableSeeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appearances')->insert([
          ['id' => 1, 'title' => "Appearance 1", 'detail' => "Location performed at", 'date' => "29/05/2018"],
          ['id' => 2, 'title' => "Appearance 2", 'detail' => "Location performed at", 'date' => "29/05/2018"],
          ['id' => 3, 'title' => "Appearance 3", 'detail' => "Location performed at", 'date' => "29/05/2018"],
        ]);
    }
}
