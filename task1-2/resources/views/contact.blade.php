<!doctype html>
<html>
<head>
  <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
  <div class="container">

      <header class="row">
          @include('includes.header')
      </header>

      <article class="row">

           @yield('content')

      </article>

  </div><!-- close container -->
  <h1>Contact us</h1>
</body>
</html>
