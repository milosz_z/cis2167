<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class appearances extends Model
{
    protected $fillable = [
      'title',
      'detail',
      'date',
    ];
}
