<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// home page
Route::resource('/', 'HomeController');

// about page
Route::resource('about', 'AboutController');

// music page
Route::resource('music', 'MusicController');


// appearances page
Route::resource('appearances', 'AppearancesController');



// contact page
Route::resource('contact', 'ContactController');


Route::group(['middle' => ['web']], function() {
  //admin appearance page
  Route::resource('appearances', 'AppearanceController');

});
