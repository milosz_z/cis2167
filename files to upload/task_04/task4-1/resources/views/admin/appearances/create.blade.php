<!doctype html>
  <html>
  <head>
      <meta charset="UTF-8">
      <title>Admin - create Appearances</title>
      <link rel="stylesheet" href="/css/app.css" />
  </head>
  <body>
  <div class="container">
      <header class="row">
          <nav class="navbar navbar-inverse navbar-fixed-top">
              <div class="container">
                  <ul class="nav navbar-nav">
                      <a class="navbar-brand" href="#">Admin</a>
                      <li class="active"><a href="/appearances">Appearances</a></li>
                  </ul>
              </div>
          </nav>
      </header>
      <article class="row">
          <h1>Create a new Appearance</h1>



          <!-- form goes here -->
        


          {!! Form::open(['url' => 'appearances']) !!}

          <div class="form-group">
            {!! Form::label('title', 'Appearance:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('detail', 'Location:') !!}
            {!! Form::text('detail', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('date', 'Date:') !!}
            {!! Form::text('date', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::submit('Add Appearance', ['class' => 'btn btn-primary form-control']) !!}
          </div>


          {!! Form::close() !!}


      </article>
      <footer class="row">
          @include('includes.footer')
      </footer>
  </div><!-- close container -->

  </body>
  </html>
