<!doctype html>
<html>
<body>
  <div class="container">

      <header class="row">
          @include('includes.header')
      </header>

      <article class="row">

           @yield('content')

      </article>

  </div><!-- close container -->
  <h1>about us</h1>
</body>
</html>
