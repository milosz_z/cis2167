<!doctype html>
<html>
<div class="container">

    <header class="row">
        @include('includes.header')
    </header>

    <article class="row">

         @yield('content')

    </article>

</div><!-- close container -->
<h1>Appearances</h1>
</body>
</html>
