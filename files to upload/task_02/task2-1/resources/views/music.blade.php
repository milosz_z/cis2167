<!doctype html>
<html>
<body>
  <div class="container">

      <header class="row">
          @include('includes.header')
      </header>

      <article class="row">

           @yield('content')

      </article>

  </div><!-- close container -->
  <h1>Our Music</h1>
  <ul>
    <li>music 1</li>
    <li>music 2</li>
  </ul>
</body>
</html>
