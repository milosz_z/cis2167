<!doctype html>
<html>
<head>
  <link rel="stylesheet" href="/css/app.css" />
</head>
<div class="container">

    <header class="row">
        @include('includes.header')
    </header>

    <article class="row">

         @yield('content')

    </article>

</div><!-- close container -->
<h1>Appearances</h1>
<section>
    @if (isset ($appearances))

        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <td>Appearance</td>
              <td>Location</td>
              <td>Date</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($appearances as $appearance)
                <tr>
                    <td>{{ $appearance->title }}</td>
                    <td>{{ $appearance->detail }}</td>
                    <td>{{ $appearance->date }}</td>
                </tr>

            @endforeach
          </tbody>
        </table>
    @else
        <p> no appearances currently </p>
    @endif
</section>
</body>
</html>
