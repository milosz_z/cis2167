<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// home page
Route::resource('/', 'HomeController');
//Route::get('/', function () {
//    return view('home');
//});


// about page
Route::resource('about', 'HomeController');
//Route::get('about', function () {
//    return view('about');
//});

// music page
Route::resource('music', 'HomeController');
//Route::get('music', function () {
//    return view('music');
//});

// appearances page
Route::resource('appearances', 'HomeController');
//Route::get('appearances', function () {
//    return view('appearances');
//});

// contact page
Route::resource('contact', 'HomeController');
//Route::get('contact', function () {
//    return view('contact');
//});
